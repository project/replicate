<?php

namespace Drupal\replicate\Events;

/**
 * Class that handles replicate of events.
 */
final class ReplicatorEvents {

  /**
   * The after save constant.
   *
   * @see \Drupal\replicate\Events\AfterSaveEvent
   */
  const AFTER_SAVE = 'replicate__after_save';

  /**
   * The replicate alter constant.
   *
   * @see \Drupal\replicate\Events\ReplicateAlterEvent
   */
  const REPLICATE_ALTER = 'replicate__alter';

  /**
   * Function to replicate entity event.
   *
   * @param int $entity_type_id
   *   The entity type id.
   *
   * @return string
   *   Returns the entity type id.
   */
  public static function replicateEntityEvent($entity_type_id) {
    return 'replicate__entity__' . $entity_type_id;
  }

  /**
   * Function to replicate entity field.
   */
  public static function replicateEntityField($field_type) {
    return 'replicate__entity_field__' . $field_type;
  }

}
