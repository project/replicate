<?php

namespace Drupal\replicate\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class to replicate event base.
 */
abstract class ReplicateEventBase extends Event {

  /**
   * The entity object.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity interface.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Function to get the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return the entity.
   */
  public function getEntity() {
    return $this->entity;
  }

}
