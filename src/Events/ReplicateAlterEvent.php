<?php

namespace Drupal\replicate\Events;

use Drupal\Core\Entity\EntityInterface;

/**
 * Class that handles replicating alter events.
 */
class ReplicateAlterEvent extends ReplicateEventBase {

  /**
   * The entity object.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $original;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity interface.
   * @param \Drupal\Core\Entity\EntityInterface $original
   *   The entity interface.
   */
  public function __construct(EntityInterface $entity, EntityInterface $original) {
    parent::__construct($entity);
    $this->original = $original;
  }

  /**
   * Gets the original entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Returns the original entity.
   */
  public function getOriginal() {
    return $this->original;
  }

}
